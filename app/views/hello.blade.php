<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>V Congreso Iberoamericano de Cultura</title>

    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('css/styles.css') }}
    {{ HTML::script('js/bootstrap.min.js') }}

    <style>
        @import url(//fonts.googleapis.com/css?family=Lato:300,400,700);
    </style>
</head>
<body>
    <div class="container" >
        <div class="row">
            <div class="col-md-12">
                <img src="img/logo.png" alt="V Congreso Iberoamericano de Cultura">
            </div>

            <div class="jumbotron" style="margin-top:-60px">
                <h1>¡Hola!</h1>
                <p>Esta web fue construida para describir la experiencia de los 21 finalistas iberoamericanos del concurso "Emprende con Cultura", dentro del V Congreso Iberoamericano de Cultura, realizado en Zaragoza, España entre los d&iacute;as 18 y 22 de noviembre de 2013.</p>

                <p>Nuestra idea, es que a trav&eacute;s de este medio, podamos generar conocimiento y sinergia, en base a nuestro propios proyectos y los que en un futuro ser&aacute;n parte de esta comunidad.</p>
                <!--p><a class="btn btn-primary btn-lg" role="button">Learn more</a></p-->
            </div>

            
            <div id="experiencia">
                <ul>
                    <li class="sebastian">
                        <p><b>Sebasti&aacute;n, Argentina: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="pedro">
                        <p><b>Pedro, Bolivia: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="amanda">
                        <p><b>Amanda, Brasil: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="jorge">
                        <p><b>Jorge, Colombia: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="elias">
                        <p><b>El&iacute;as, Costa  Rica: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="duni">
                        <p><b>Dunieska, Cuba: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="felipe">
                        <p><b>Felipe, Chile: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="pablo">
                        <p><b>Pablo, Ecuador: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="german">
                        <p><b>Germ&aacute;n, El Salvador: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="anxo">
                        <p><b>Anxo, España: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="daniel">
                        <p><b>Daniel, Guatemala: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="didier">
                        <p><b>Didier, Honduras: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="aldo">
                        <p><b>Aldo, M&eacute;xico: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="mario">
                        <p><b>Mario, Nicaragua: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="elmer">
                        <p><b>Elmer, Panam&aacute;: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="alejandro">
                        <p><b>Alejandro, Paraguay: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="alvaro">
                        <p><b>&Aacute;lvaro, Per&uacute;: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="tiago">
                        <p><b>Tiago, Portugal: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="alejandro-2">
                        <p><b>Alejandro, Rep&uacute;blica Dominicana: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="maria">
                        <p><b>Mar&iacute;a, Uruguay: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                    <li class="martin">
                        <p><b>Mart&iacute;n, Venezuela: </b>Me encant&oacute; participar en el congreso. Volver&iacute;a para ver la Dama China, El Zorro, comer tapas, tomar vino y cerveza y verlos a todos de nuevo :)</p>
                    </li>
                </ul>
            </div>
        </div>    
    </div>
    <div id="footer">
      <div class="container">
        <p>Hecho desde Chile con amor :) - <?php echo date('Y') ?></p>
      </div>
    </div>
</body>
</html>
